
CREATE TABLE [dbo].[Pizza](
[id] [int] NOT NULL,
[name] [varchar](50) NULL,
[ingredients] [varchar](50) NULL,
CONSTRAINT [PK_Recipes] PRIMARY KEY CLUSTERED
(
[id] ASC
)
) ON [PRIMARY]
CREATE TABLE [dbo].[Ingredients](
[id] [int] NOT NULL,
[name] [varchar](40) NULL,
CONSTRAINT [PK_Ingredients] PRIMARY KEY CLUSTERED
(
[id] ASC
)
) ON [PRIMARY]
CREATE TABLE [dbo].[Orders](
[order_id] [int] NOT NULL,
[customer_id] [int] NULL,
[pizza_id] [int] NULL,
[exclusions] [varchar](4) NULL,
[extras] [varchar](4) NULL,
[order_time] [datetime] NULL
) ON [PRIMARY]
INSERT [dbo].[Ingredients] ([id], [name]) VALUES (1, N'Bacon')
INSERT [dbo].[Ingredients] ([id], [name]) VALUES (2, N'BBQ Sauce')
INSERT [dbo].[Ingredients] ([id], [name]) VALUES (3, N'Beef')
INSERT [dbo].[Ingredients] ([id], [name]) VALUES (4, N'Cheese')
INSERT [dbo].[Ingredients] ([id], [name]) VALUES (5, N'Chicken')
INSERT [dbo].[Ingredients] ([id], [name]) VALUES (6, N'Mushrooms')
INSERT [dbo].[Ingredients] ([id], [name]) VALUES (7, N'Onions')
INSERT [dbo].[Ingredients] ([id], [name]) VALUES (8, N'Pepperoni') 
INSERT [dbo].[Ingredients] ([id], [name]) VALUES (9, N'Peppers')
INSERT [dbo].[Ingredients] ([id], [name]) VALUES (10, N'Salami')
INSERT [dbo].[Ingredients] ([id], [name]) VALUES (11, N'Tomatoes')
INSERT [dbo].[Ingredients] ([id], [name]) VALUES (12, N'Tomato Sauce') 
INSERT [dbo].[Ingredients] ([id], [name]) VALUES (13, N'Prosciutto')
INSERT [dbo].[Orders] ([order_id], [customer_id], [pizza_id], [exclusions], [extras], [order_time]) VALUES
(1, 101, 1, N'', N'', CAST(N'2021-02-01T18:05:02.000' AS DateTime))
INSERT [dbo].[Orders] ([order_id], [customer_id], [pizza_id], [exclusions], [extras], [order_time]) VALUES
(2, 101, 1, N'', N'', CAST(N'2021-02-01T19:00:52.000' AS DateTime))
INSERT [dbo].[Orders] ([order_id], [customer_id], [pizza_id], [exclusions], [extras], [order_time]) VALUES
(3, 102, 1, N'', N'', CAST(N'2021-03-02T23:51:23.000' AS DateTime))
INSERT [dbo].[Orders] ([order_id], [customer_id], [pizza_id], [exclusions], [extras], [order_time]) VALUES
(3, 102, 2, N'', NULL, CAST(N'2021-03-02T23:51:23.000' AS DateTime))
INSERT [dbo].[Orders] ([order_id], [customer_id], [pizza_id], [exclusions], [extras], [order_time]) VALUES
(4, 103, 1, N'4', N'', CAST(N'2021-05-04T13:23:46.000' AS DateTime))
INSERT [dbo].[Orders] ([order_id], [customer_id], [pizza_id], [exclusions], [extras], [order_time]) VALUES
(4, 103, 2, N'4', N'', CAST(N'2021-05-04T13:23:46.000' AS DateTime))
INSERT [dbo].[Orders] ([order_id], [customer_id], [pizza_id], [exclusions], [extras], [order_time]) VALUES
(5, 104, 1, N'null', N'1', CAST(N'2021-06-08T21:00:29.000' AS DateTime))
INSERT [dbo].[Orders] ([order_id], [customer_id], [pizza_id], [exclusions], [extras], [order_time]) VALUES
(6, 101, 2, N'null', N'null', CAST(N'2021-06-08T21:03:13.000' AS DateTime))
INSERT [dbo].[Orders] ([order_id], [customer_id], [pizza_id], [exclusions], [extras], [order_time]) VALUES
(7, 105, 2, N'null', N'1', CAST(N'2021-06-08T21:20:29.000' AS DateTime))
INSERT [dbo].[Orders] ([order_id], [customer_id], [pizza_id], [exclusions], [extras], [order_time]) VALUES
(8, 102, 1, N'null', N'null', CAST(N'2021-07-09T23:54:33.000' AS DateTime))
INSERT [dbo].[Orders] ([order_id], [customer_id], [pizza_id], [exclusions], [extras], [order_time]) VALUES
(9, 103, 1, N'4', N'1, 5', CAST(N'2021-08-10T11:22:59.000' AS DateTime))
INSERT [dbo].[Orders] ([order_id], [customer_id], [pizza_id], [exclusions], [extras], [order_time]) VALUES
(10, 104, 1, N'null', N'null', CAST(N'2021-09-11T18:34:49.000' AS DateTime))
INSERT [dbo].[Orders] ([order_id], [customer_id], [pizza_id], [exclusions], [extras], [order_time]) VALUES
(10, 104, 1, N'2, 6', N'1, 4', CAST(N'2021-09-11T18:34:49.000' AS DateTime))
INSERT [dbo].[Orders] ([order_id], [customer_id], [pizza_id], [exclusions], [extras], [order_time]) VALUES
(11, 115, 3, NULL, N'8', CAST(N'2021-09-11T19:45:29.000' AS DateTime))
INSERT [dbo].[Pizza] ([id], [name], [ingredients]) VALUES (1, N'Carnivore', N'1, 2, 3, 4, 5, 6, 8, 10')
INSERT [dbo].[Pizza] ([id], [name], [ingredients]) VALUES (2, N'Vegetarian', N'4, 6, 7, 9, 11, 12')
INSERT [dbo].[Pizza] ([id], [name], [ingredients]) VALUES (3, N'Prosciutto e sql', N'11, 12, 13, 6') 


--                             OBJECTIVE
--
--   ---Given the table schemas below, write a query to print a new pizza recipe that includes
--      the most used 5 ingredients in all the ordered pizzas in the past 6 months.
--
--   ---Help the cook by generating an alphabetically ordered comma separated ingredient list
--      for each ordered pizza and add a 2x in front of any ingredient that is requested as extra
--      and is present in the standard recipe too. 
---------------------------------------------------------------------
--                            TASKS
-- 
-- 1- Clean Tables (Create a row Index to have an unique way to track all orders)
-- 2- Split comma Separated Ingredients to rows and remove blank spaces
-- 3- Split comma Separated Extras to rows nd remove blank spaces
-- 4- Create list with full ingredient 
-- 5- Join Result with Ingredients table
-- 6.1- To reach 1st objective, count all Ingredients and obtain the first 5 order by the count desc
--      Concatenate all ingredients names obtained
        
-- 6.2- To reach second objective, count ingredients by row Index 
--      Join full cleaned order on Index()
--      Concatenate all ingredients name grouping by index
---------------------------------------------------------------------
-------
-- 1 --
-------
SELECT
ROW_NUMBER() OVER(ORDER BY order_id ASC) AS rowIndex
,[order_id]
,[customer_id]
,[pizza_id]
,CASE
WHEN[extras] IN ('', 'NULL') THEN NULL
ELSE [extras]
END AS [extras]
,[order_time]  
INTO #CleanOrders 
FROM [dbo].[Orders]

-------
-- 2 --
-------

SELECT 
 [id]
,[name]
,value
INTO #SplitPizza
from [dbo].[Pizza]
CROSS APPLY STRING_SPLIT(ingredients, ',');


--- Clean the the value field from spaces
SELECT  
 [id]
,[name]
,trim([value]) AS ingredientsId
INTO #CleanSplitPizza
FROM #SplitPizza

---  Join the table CleanSplitPizza with the table CleanOrders
 SELECT
     #CleanOrders.[rowIndex] 
    ,#CleanOrders.[order_id]
    ,#CleanOrders.[customer_id]
    ,#CleanOrders.[pizza_id]
    ,#CleanOrders.[extras]
    ,#CleanSplitPizza.[ingredientsId]
  INTO #OrderWithPizzaID  
  FROM #CleanOrders 
  LEFT JOIN #CleanSplitPizza
    ON #CleanOrders.[pizza_id] = #CleanSplitPizza.[id]
-------
-- 3 --
-------
--- We see what order has an extra ingredient
SELECT
 [rowIndex] 
,[order_id]
,[customer_id]
,[pizza_id]
,[extras]
INTO #ExtraIngredientOrders 
FROM #CleanOrders
WHERE #CleanOrders.[extras] IS NOT NULL

--- Split the comma separated string of ExtraIngredientOrders to rows
select
 [rowIndex] 
,[order_id]
,[customer_id]
,[pizza_id]
,[value]
,[extras]
INTO #ExtraIngredientOrdersSplit 
FROM #ExtraIngredientOrders
CROSS APPLY STRING_SPLIT(extras, ',')

--- Trim the value field and change field name to  ingredientsId
SELECT
 [rowIndex] 
,[order_id]
,[customer_id]
,[pizza_id]
,[extras]
,TRIM([value]) as [ingredientsId]
INTO #CleamExtraIngredientOrders 
FROM #ExtraIngredientOrdersSplit



-------
-- 4 --
-------
--- Create the full list of ingredients
SELECT oil.* INTO #OrderIngredientsList
FROM(
SELECT 
 [rowIndex] 
,[order_id]
,[customer_id]
,[pizza_id]
,[extras]
,[ingredientsId]
FROM #CleamExtraIngredientOrders
UNION ALL
SELECT 
 [rowIndex] 
,[order_id]
,[customer_id]
,[pizza_id]
,[extras]
,[ingredientsId]
FROM #OrderWithPizzaID
) oil

-------
-- 5 --
-------
--- Join Ingredients Name
SELECT
     #OrderIngredientsList.[rowIndex]
    ,#OrderIngredientsList.[order_id]
    ,#OrderIngredientsList.[customer_id]
    ,#OrderIngredientsList.[pizza_id]
    ,#OrderIngredientsList.[extras]
    ,#OrderIngredientsList.[ingredientsId]
    ,[dbo].[Ingredients].[name]
  INTO #OrderIngredientsNameList  
  FROM #OrderIngredientsList 
  LEFT JOIN [dbo].[Ingredients]
    ON #OrderIngredientsList.[ingredientsId] =  [dbo].[Ingredients].[id]

-------
--5.1--
-------
-- Obtain the 5 more used ingredients(including extras)
SELECT TOP 5
 [ingredientsId] 
,[name]
, COUNT(ingredientsId) AS amountOfIngredient
INTO #NewRecipeIngredients
FROM #OrderIngredientsNameList
GROUP BY ingredientsId, name
ORDER BY amountOfIngredient DESC

-- Concatenate all ingredients with a comma to create teh new recipe
SELECT 
STRING_AGG([name], ',') AS new_recipe
INTO #NewRecipe
FROM   #NewRecipeIngredients

-------
--5.2--
-------

-----Add the amount of ingrdients 
SELECT 
 [rowIndex]
,[order_id]
,[customer_id]
,[pizza_id]
,[extras]
,[ingredientsId] 
,[name]
, COUNT(ingredientsId) AS amountOfIngredient
INTO #OrderIngredientsNameAscList 
FROM  #OrderIngredientsNameList 
group by rowIndex, order_id,pizza_id,name, ingredientsId, customer_id, extras 

--- Create Recipies with order number
SELECT 
 [rowIndex]
,[order_id]
,STRING_AGG(CASE WHEN amountOfIngredient > 1 THEN cast(amountOfIngredient AS varchar) + 'x'  + name ELSE name END, ',') WITHIN GROUP ( ORDER BY order_id asc,pizza_id asc, name asc)AS ingredients_list
INTO #OrderRecipeList
FROM   #OrderIngredientsNameAscList 
GROUP BY [rowIndex],[order_id]

--- Join recipes of all orders to the cleaned order tables to keep all format and add extra recipee collum

SELECT
     #CleanOrders.[order_id]
    ,#CleanOrders.[customer_id]
    ,#CleanOrders.[pizza_id]
    ,#CleanOrders.[extras]
    ,#CleanOrders.[order_time]
    ,#OrderRecipeList.[ingredients_list]
  INTO #FullOrderWithRecipe 
  FROM #CleanOrders 
  LEFT JOIN #OrderRecipeList 
    ON #CleanOrders.[rowIndex] =  #OrderRecipeList.[rowIndex]

----------------------------  Run these querys to obtain answer
select * from #NewRecipe
select * from #FullOrderWithRecipe 

